# Cognito Federated SSO Template

## Parameters

| Parameter Name | Description | Example / *Default Value* |
| -- | -- | -- |
| **DomainName** | An Domain that will be prefixed to Pool Url and other Resources | `orderhiveSSOtest` |
| **FederatedProviderName** | Name for the Federated Identity Provider | *`azuread`* |
| **SupportedIdentityProviders** | Supported Identity Providers for your UserPool | *`COGNITO`* |
| **MetadataURL** | Metadata URL Provided by Federation Source | *`http://localhost`* |
| **CallBackURLs** | Comma Delimited List of  Call Back URLs for the Application Client | `"http://localhost, http://localhost:8888"` |
| **LogoutURLs** | Comma Delimited List of  LogoutURLs for the Application Client | `"http://localhost, http://localhost:8888"` |

--------------------------------
###  Example Parameters.json to be Used with CLI

```json
[
    {
        "ParameterKey": "DomainName",
        "ParameterValue": "orderhive-sso"
    },
    {
        "ParameterKey": "SupportedIdentityProviders",
        "ParameterValue": "COGNITO"

    },
    {
        "ParameterKey" : "FederatedProviderName",
        "ParameterValue" : "azuread"
    },
    {
        "ParameterKey": "MetadataURL",
        "ParameterValue": "https://login.microsoftonline.com/25492ca8-e41c-40f0-8210-c8a366016407/federationmetadata/2007-06/federationmetadata.xml?appid=70d67fa1-1d19-4d71-9725-ca5601a25018"
    },
    {
        "ParameterKey": "CallBackURLs",
        "ParameterValue": "https://localhost:8080"
    },
    {
        "ParameterKey": "LogoutURLs",
        "ParameterValue": "https://localhost:8080"
    }
]
```
-------
# Applying the Stack 

## From AWS CLI

### 1. Initalize the stack.

```bash
# export TEMPLATE_URL=https://cloudformationproduction.s3.amazonaws.com/cognito-sso/cognito.yaml
export TEMPLATE_URL=file://cognito.yaml
export DOMAIN_NAME="test-sso"
export SUPPORTED_IDP="COGNITO"
export FEDERATED_PROVIDER_NAME="azuread"
export CALLBACK_URLS="http://localhost"
export LOGOUT_URLS="http://localhost"

aws cloudformation create-stack --stack-name $DOMAIN_NAME --template-body $TEMPLATE_URL  \
--parameters \
ParameterKey=DomainName,ParameterValue=$DOMAIN_NAME \
ParameterKey=SupportedIdentityProviders,ParameterValue=$SUPPORTED_IDP \
ParameterKey=FederatedProviderName,ParameterValue=$FEDERATED_PROVIDER_NAME \
ParameterKey=CallBackURLs,ParameterValue=$CALLBACK_URLS \
ParameterKey=LogoutURLs,ParameterValue=$LOGOUT_URLS \
--capabilities CAPABILITY_IAM

```
### 2. Get Output

```bash
aws cloudformation describe-stacks --stack-name $DOMAIN_NAME | jq '.Stacks[].Outputs[] | [.OutputKey, .OutputValue]'
```
### 3.  Add MetadataURL:

```bash
# Replace with Actual value from provider.
export METADATA_URL=https://login.microsoftonline.com/25492ca8-e41c-40f0-8210-c8a366016407/federationmetadata/2007-06/federationmetadata.xml?appid=70d67fa1-1d19-4d71-9725-ca5601a25018
aws cloudformation update-stack --stack-name $DOMAIN_NAME --template-body $TEMPLATE_URL  \
--parameters \
ParameterKey=MetadataURL,ParameterValue=$METADATA_URL \
ParameterKey=DomainName,ParameterValue=$DOMAIN_NAME \
ParameterKey=SupportedIdentityProviders,ParameterValue=$SUPPORTED_IDP \
ParameterKey=FederatedProviderName,ParameterValue=$FEDERATED_PROVIDER_NAME \
ParameterKey=CallBackURLs,ParameterValue=$CALLBACK_URLS \
ParameterKey=LogoutURLs,ParameterValue=$LOGOUT_URLS \
--capabilities CAPABILITY_IAM
```

### 4. Update Supported Identity Providers:

```bash
export METADATA_URL=https://login.microsoftonline.com/25492ca8-e41c-40f0-8210-c8a366016407/federationmetadata/2007-06/federationmetadata.xml?appid=70d67fa1-1d19-4d71-9725-ca5601a25018
export SUPPORTED_IDP=$FEDERATED_PROVIDER_NAME

aws cloudformation update-stack --stack-name $DOMAIN_NAME --template-body $TEMPLATE_URL  \
--parameters \
ParameterKey=MetadataURL,ParameterValue=$METADATA_URL \
ParameterKey=DomainName,ParameterValue=$DOMAIN_NAME \
ParameterKey=SupportedIdentityProviders,ParameterValue=$SUPPORTED_IDP \
ParameterKey=FederatedProviderName,ParameterValue=$FEDERATED_PROVIDER_NAME \
ParameterKey=CallBackURLs,ParameterValue=$CALLBACK_URLS \
ParameterKey=LogoutURLs,ParameterValue=$LOGOUT_URLS \
--capabilities CAPABILITY_IAM
```
## From AWS Console
### Login to your AWS Console and Open Cloudformation.

### Click on Create Stack and Choose "With New Resources (Standard)

---

![Login and Open Cloudformation](./media/goto-cf.gif)

---

### In **Specify Template** Section, select `Upload a template file` as template source, and Upload the template file. i.e [`cognito.yaml`](.cognito.yaml) and Click **NEXT**.
---

![Specify Template](./media/specify-template.gif)

---

### In **Specify stack details** Provide the required detail, as given in example and Click **NEXT**

- Unless You have the actual MetadataURL, during Initial Creation , keep the Following Parameters to Default.
        
    * SupportedIdentityProviders
    * MetadataURL
    > Note: Because Upon Initial Stack Creation **`Identifier`**, **`ReplyURL`**,  **`ClientID`**, **`ClientSecret`**  will be generated, which are required for the Provider to Generate the Actual Metadata URL. Once you have the actual metadata URL, update the stack and update the above Parameters with corresponding values. Set **`SupportedIdentityProviders`** Parameter to include the same value as **`FederatedProviderName`**
---

![Set Parameters](./media/set-parameters.gif)

---

### In **Configure stack options**, optionally provide the tags, but its not necessary, and keep everything else as it is and Click **NEXT**

---

![Configure Options](./media/configure-options.gif)

---

###  Finally on the **Review** page, review your settings and scroll down to the capabilites section and Check `I acknowledge that AWS CloudFormation might create IAM resources.`

### Now that you have reviewed all your configurtion Click on **Create Stack** to begin resource creation.

---

![Create Stack](./media/create-stack.gif)

---

![Creation Status](./media/creation-status.gif)

---
- After Successful Stack Creation check the outputs and get **`Identifier`** and **`ReplyURL`** **`ClientID`** .**`ClientSecret`** has to be read manually from the Cognito Client Configuration using Web Console. Use these values to configure on the Provider End, and upon configuration you'll have the **`MetadataURL`**. 

## Updating the Stack with actual SAML metadata url from the provider. (Update Step 1)

- Login to Console and Open Cloudformation.

- Select the stack you previously created from the stack list and click on **`Update`**. Select to Update from **`Current Template`**.

![Update Stack](./media/update-stack.png) 

- Update The Parameters and Set MetadataURL using actual value.

![Update Parameters](./media/update-parameters.png)

- Keep the Configuration Options unchanged, and review your changes.

- Check the Change Set Preview and Here you'll obeserve that UserPoolIdentityProvider will be added.

![Review Changes](./media/review-changes.png)

- Apply the Changes and this should create a User Pool Identity Provider with FederatedProviderName Parameter you provided.

- In the Next Step you need to Update the Stack and Update the value of **`SupportedIdentityProviders`** to the one we provided for **`FederatedProviderName`**.

## Updating the Stack with Update in Value for SupportedIdentityProviders. (Update Step 2)

- Login to Console and Open Cloudformation.

- Select the stack you previously created from the stack list and click on **`Update`**. Select to Update from **`Current Template`**.

- Update The Parameters and Set **`SupportedIdentityProviders`** using actual value. It should be same as one used for **`FederatedProviderName`** i.e `azuread`

- Keep the Configuration Options unchanged, and review your changes.

- Apply the Changes and now your Congito User Pool is configured to use the Federated Identity Provider.